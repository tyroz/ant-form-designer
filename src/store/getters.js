const getters = {
  routers: state => state.permission.routers,
  active: state => state.permission.active,
  leftActive: state => state.permission.leftActive
}

export default getters
